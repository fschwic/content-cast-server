#!/bin/bash

if [ "x$1" == "x" ] ; then
        echo Usage: $0 file
        exit 1;
fi

. PodcastDir.conf
. functions.sh

FILE=$1

file_name=`$BIN_BASENAME "$FILE"`
file_rel_path=${FILE/$BASE_DIR/}
file_dir=${FILE/$file_name/}

file_title=${file_name/\.*/}
file_title=${file_title//_/\ }
# FIXME just spaces in filename are encoded
file_url=${BASE_URL}${file_rel_path/$file_name/${file_name//\ /%20}}
rss2_file=${PODCAST_DIR}${file_rel_path}.rss2
rss2_file_dir=${PODCAST_DIR}${file_rel_path/$file_name/}
podcast_url=${PODCAST_URL}${file_rel_path/$file_name/}
pubdate_offset=""

# evaluate per directory settings
overwrite="false"
readDirectoryText "${rss2_file_dir}"
if [ "x${DIRECTORY[overwrite]}" = "xall" ]; then
    echo "overwrite is set to 'all'" 1>&2
    overwrite="true"
else
    #echo "overwrite is not set to 'all'" 1>&2
    overwrite="false"
fi

# if title/filename starts with number, remove number from title and put pubdate number minutes in past
comp=`echo $file_title | sed -e "s/^[0-9][0-9]*[\ _\-]//"`
if [ "$comp" != "$file_title" ]; then
    # title  without leading digits and underscores and hypens
    tmp=${comp}
    # the leading which is removed, now
    num=`echo $file_title | sed -e "s/${tmp}$//"`
    # remove leading zero digit
    num=`echo $num | sed -e "s/^00*//"`
    # remove trailing non-digits
    pubdate_offset=`echo $num | sed -e "s/[^0-9]*$//"`
    
    # if keep-numbers don't remove number from title
    if [ "x${DIRECTORY[keep-numbers]}" != "xyes" ]; then
	file_title=$tmp
    fi
fi

if [ $overwrite = "true" -o ! -e "$rss2_file" ]; then
    mime=`mimeType "$FILE"`
    thumbnail_url=`$BIN_DIR/create-thumbnail-${mime/\/*/}.sh "$FILE"`

    # mime workaround for some mp3 files stated as application/octet-stream
    if [ $mime == "application/octet-stream" ] ; then
	case $FILE in
	    *.mp3) 
		mime="audio/mpeg"
		;;
	    *) 
	    # leave mime as it is
		;;
	esac
    fi

    # create directory for RSS snippets
    $BIN_MKDIR -p "$rss2_file_dir"
    pDate=$(pubDate "$FILE" $pubdate_offset)
    # write RSS2 snippet
    $BIN_DIR/default-item.sh "$file_title" "" "$pDate" "$file_url" "$mime" `length "$FILE"` "$podcast_url" "$thumbnail_url" > "$rss2_file"
fi
