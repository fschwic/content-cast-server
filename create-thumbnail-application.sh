#!/bin/bash

if [ "x$1" == "x" ] ; then
        echo Usage: $0 file
        exit 1;
fi

. PodcastDir.conf
. functions.sh

FILE=$1
mime=`mimeType "$FILE"`

case $FILE in
     *.mp3) 
	$BIN_DIR/create-thumbnail-audio.sh "$FILE"
	;;
     *) 
	$BIN_DIR/create-thumbnail-${mime/*\//}.sh "$FILE"
	;;
esac
