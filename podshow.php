<?php

// XML-Quellen laden
$xml = new DOMDocument;
$xml->load($_GET['cast']);

$xsl = new DOMDocument;
$xsl->load('xslt/podshow.xsl');

// Prozessor instanziieren und konfigurieren
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // XSL Document importieren
$proc->setParameter('', 'cast', $_GET['cast']);
if(isset($_GET['n'])){
	$proc->setParameter('', 'n', $_GET['n']);
}

$proc->transformToURI($xml, 'php://output');

?>
