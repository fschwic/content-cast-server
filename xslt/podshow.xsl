<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:pdccs="http://schwichtenberg.net/pdccs/2008/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rss="http://purl.org/rss/1.0/" xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:cv="http://3iii.net/cv">
  <xsl:output encoding="iso-8859-1" indent="yes" method="html" />

  <xsl:param name="cast"/>
  <xsl:param name="n">1</xsl:param>

  <xsl:template match="channel">
    <xsl:variable name="c" select="count(item)"/>
    <xsl:variable name="next_n"><xsl:value-of select="($n mod $c)+1"/></xsl:variable>
    <xsl:variable name="next_url">podshow.php?cast=<xsl:value-of select="$cast"/>&amp;n=<xsl:value-of select="$next_n"/></xsl:variable>
    <html>
      <head>
	<title><xsl:value-of select="title"/></title>
	<meta http-equiv="refresh">
	  <xsl:attribute name="content">3; URL=<xsl:value-of select="$next_url"/></xsl:attribute>
	</meta>
	<style>
         body{background-color:black; color:#dddddd;font-family:sans-serif;}
         a{color:#cccccc;}
         .feed_item{background-color:#444444;padding:10px;margin:10px;float:left;}
	</style>
      </head>
      <body>
	<xsl:apply-templates select="item[position() = $n]">
	  <xsl:with-param name="next_url" select="$next_url"/>
	</xsl:apply-templates>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="item">
    <xsl:param name="next_url"/>
    <xsl:apply-templates select="enclosure">
	  <xsl:with-param name="next_url" select="$next_url"/>
    </xsl:apply-templates>
    <b><xsl:value-of select="title"/></b><br/>
    (<xsl:value-of select="pubDate"/>)<br/>
    <xsl:if test="description">
      <xsl:value-of select="description"/><br/>
    </xsl:if>
    <br/>
    aus <a>
      <xsl:attribute name="href"><xsl:value-of select="$cast"/></xsl:attribute>
      <xsl:value-of select="/rss/channel/title"/>
    </a><br/>
  </xsl:template>

  <xsl:template match="enclosure[starts-with(@type,'image/')]">
    <xsl:param name="next_url"/>
    <a>
      <xsl:attribute name="href"><xsl:value-of select="$next_url"/></xsl:attribute>
      <img style="height:98%; float:left; margin-right:10px" border="0">
	<xsl:attribute name="src"><xsl:value-of select="@url"/></xsl:attribute>
      </img>
    </a>
    <script type="text/javascript">
      image = document.getElementsByTagName("img")[0];
      <xsl:if test="../media:thumbnail">
	image.lowsrc = "<xsl:value-of select="../media:thumbnail/@url"/>"
      </xsl:if>
    </script>
  </xsl:template>

  <xsl:template match="enclosure[starts-with(@type,'video/')]">
    <xsl:param name="next_url"/>
    <xsl:choose>
      <xsl:when test="../media:thumbnail">
	<a>
	  <xsl:attribute name="href"><xsl:value-of select="@url"/></xsl:attribute>
	  <img style="float:left; margin-right:10px" border="0">
	    <xsl:attribute name="src"><xsl:value-of select="../media:thumbnail/@url"/></xsl:attribute>
	  </img>
	</a>
      </xsl:when>
      <xsl:otherwise>
	<a>
	  <xsl:attribute name="href"><xsl:value-of select="$next_url"/></xsl:attribute>
	  <video controls="controls" style="float:left; margin-right:10px" border="0">
	    <xsl:attribute name="src"><xsl:value-of select="@url"/></xsl:attribute>
	  </video>
	</a>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="enclosure">
    <p>Sorry, unknown media type.</p>
  </xsl:template>

</xsl:stylesheet>
