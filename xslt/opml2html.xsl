<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet 
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output 
      method="html"
      encoding="ISO-8859-1"
      />

  <xsl:template match="opml">
    <html>
      <xsl:apply-templates/>
    </html>
  </xsl:template>

  <xsl:template match="head">
    <!--
       <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
       <meta name="DC.title" content="Services to Government" />
    -->
    <head>
      <title><xsl:value-of select="title"/></title>
      <link rel="schema.OPML" href="http://opml.org/spec2"/>
      <xsl:for-each select="./*">
	<meta>
	  <xsl:attribute name="name">OPML.<xsl:value-of select="local-name()"/></xsl:attribute>
	  <xsl:attribute name="content"><xsl:value-of select="."/></xsl:attribute>
	</meta>
      </xsl:for-each>
    </head>
  </xsl:template>

  <xsl:template match="body">
    <body>
      <h1>
        <img src="/Podcast.bin/pics/opml-icon-32x32.png" alt="opml file / directory"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="../head/title"/>
      </h1>
      <xsl:apply-templates select="../head/description"/> 
      <hr width="100%" size="1"/>
      <xsl:apply-templates select="outline">
        <xsl:sort select="@text"/>
      </xsl:apply-templates>
    </body>
  </xsl:template>

  <!-- erst Verzeichnisse, dann Podcasts
  <xsl:template match="body">
    <body>
      <h1>
        <img src="/mp3/pics/opml-icon-32x32.png" alt="opml file / directory"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="../head/title"/>
      </h1>
      <ul>
        <a href="#Verzeichnisse">Verzeichnisse</a> - <a href="#Feeds">Feeds</a>
      </ul>
      <hr width="100%" size="1"/>
      <a name="Verzeichnisse"/>
      <h3>Verzeichnisse</h3>
      <xsl:apply-templates select="outline[@type='link']"/>
      <a name="Feeds"/>
      <h3>Feeds</h3>
      <xsl:apply-templates select="outline[@type='rss']"/>
    </body>
  </xsl:template>
  -->

  <xsl:template match="head/description">
      <p><xsl:value-of select="."/></p>
  </xsl:template>

  <xsl:template match="outline[@type='include' or @type='link']">
    <img src="/Podcast.bin/pics/opml-icon-16x16.png" alt="opml file / directory"/>
    <xsl:text> </xsl:text>
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="@url"/>
      </xsl:attribute>
      <xsl:value-of select="@text"/>
    </a>
    <br/>
  </xsl:template>

  <xsl:template match="outline[@type='rss']">
    <xsl:choose>
      <xsl:when test="@version='RSS2' or @version='rss2'">
        <img src="/Podcast.bin/pics/feed-icon-14x14.png" alt="podcast"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="@type"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="@xmlUrl"/>
      </xsl:attribute>
      <xsl:value-of select="@text"/>
    </a>
    <br/>
  </xsl:template>

  <!--
  <xsl:template match="opml">
  </xsl:template>
  <xsl:template match="opml">
  </xsl:template>
  <xsl:template match="opml">
  </xsl:template>
  <xsl:template macht="opml">
  </xsl:template>
  -->

</xsl:stylesheet>

