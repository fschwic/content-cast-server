<?xml version="1.0" encoding="UTF-8"?>
<!--
 RSS2 to HTML v1.0.3
 
 XSLT to create HTML from Podcasts (RSS2 documents).
 Every item of the podcast should have a thumbnail defined (media:thumbnail).
 
 copyright 2011 Frank Schwichtenberg <http://frank.schwichtenberg.net>
 
 You may freely copy, use, and modify this XSLT if you don't remove my name
 and the origin location of this XSLT.
 
 The origin location of this XSLT is

 Here you may find new versions.
 -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:pdccs="http://schwichtenberg.net/pdccs/2008/" 
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rss="http://purl.org/rss/1.0/" xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:cv="http://3iii.net/cv">
  <xsl:output encoding="iso-8859-1" indent="yes" method="html" />

  <xsl:include href="fschwic-string.xsl"/>

  <xsl:param name="SHOW"></xsl:param>
  <xsl:param name="xml"></xsl:param>
  <xsl:param name="xslt"></xsl:param>

  <xsl:template match="/rss">
    <html>
      <xsl:apply-templates select="channel"/>
    </html>
  </xsl:template>

  <xsl:template match="channel">
    <head>
      <title><xsl:value-of select="title"/></title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet"/>
      <script src="//code.jquery.com/jquery.js"></script>
      <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
      <script>
	function doConfirmed(doer, name){
	  if(confirm("Wirklich löschen: " + name + "?"))
	    doer(name);
	}

	function deleteEntry(name){
	  $.ajax({
	    url: name + ".rss2",
	    type: 'DELETE',
	    success: function(result) {
	      console.log(name + " gelöscht!");
	    },
	    error: function(jqXHR, msg) {
	      alert("Fehler: " + msg + ";" + jqXHR.responseText);
	      var err = eval("(" + xhr.responseText + ")");
	      alert(err.Message);
	    },
	    complete: function(jqXHR, msg) {
	      //alert("completed");
	    }
	  });
	}
      </script>
      <style>
	#body{background-color:black; color:#dddddd;font-family:sans-serif;}
	#a{color:#cccccc;}
	#.feed_item div {text-align:center;}
	header img {float:right; width:144px;}
	.feed_item h4 {margin-bottom:0px;}
	.feed_item .pubDate {font-size:.688em; line-height:normal; margin:0px;}
	.feed_item .creator {font-size:.688em; line-height:normal; margin:0px;}
	.feed_item .description {font-size:1em; line-height:normal; margin-top:5px;}
      </style>
    </head>
    <body>
      <xsl:if test="not(../@version = '2.0')">
        <xsl:comment>
          No version information found. Assuming RSS Version 2.0.
        </xsl:comment>
      </xsl:if>

      <div class="container">
	<xsl:if test="guid">
	  <xsl:attribute name="id">
	    <xsl:value-of select="guid"/>
	  </xsl:attribute>
	</xsl:if>
	
	<header class="hero-unit">
	  <xsl:if test="image">
	    <a border="0">
	      <xsl:attribute name="href"><xsl:value-of select="image/link"/></xsl:attribute>
	      <img class="img-rounded">
		<xsl:attribute name="src"><xsl:value-of select="image/url"/></xsl:attribute>
		<xsl:attribute name="alt"><xsl:value-of select="image/title"/></xsl:attribute>
	      </img>
	    </a>
	  </xsl:if>
	  <h1 class="title">
	    <a>
	      <xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>
	      <xsl:value-of select="title"/>
	    </a>
	  </h1>
	  <p>
	    <span class="md">
	      <xsl:if test="description">
		<xsl:value-of select="description"/><br/>
	      </xsl:if>
	      <xsl:if test="managingEditor">
		by <xsl:value-of select="managingEditor"/>
	      </xsl:if>
	      <xsl:if test="pubDate">
		(<xsl:value-of select="pubDate"/>)<br/>
	      </xsl:if>
	    </span>
	    <xsl:apply-templates select="atom:link[@rel = 'self']"/>
	  </p>
	</header>
	
	<xsl:choose>
	  <xsl:when test="$SHOW">
	    <xsl:apply-templates select="item[guid/text() = $SHOW]" mode="iter"/>
	  </xsl:when>
	  <xsl:when test="pdccs:first">
	    <xsl:variable name="FIRST">
	      <xsl:value-of select="pdccs:first/@rdf:resource"/>
	    </xsl:variable>
	    <xsl:apply-templates select="item[guid/text() = $FIRST]" mode="index"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <ul class="thumbnails">
	      <xsl:variable name="MONTHES" select="document('datetime.xml')/datetime"/>
		<!-- TODO very expensive! Use an appropriate datetime string in ContentCasts and choose if available. -->
	      <xsl:apply-templates select="item">
		<xsl:sort select="concat(substring(pubDate,13,4),$MONTHES/month[@abbr=substring(current()/pubDate,9,3)]/@nn,substring(pubDate,6,2),substring(pubDate,18,8))"/>
	      </xsl:apply-templates>
	    </ul>
	  </xsl:otherwise>
	</xsl:choose>
	
      </div>
    </body>
  </xsl:template>

  <xsl:template match="atom:link[@rel = 'self']">
    <p>
      <a>
	<xsl:attribute name="href">http://frank.schwichtenberg.net/podcast-swipe/?rss=<xsl:value-of select="@href"/></xsl:attribute>
	<b>Swipe it!</b>
      </a>
    </p>
  </xsl:template>
  
  <xsl:template match="item">
    <li class="span4 feed_item">
      <xsl:if test="guid">
        <xsl:attribute name="id">
          <xsl:value-of select="guid"/>
        </xsl:attribute>
      </xsl:if>

      <div class="thumbnail">

      <!-- preview -->
      <xsl:choose>
	<xsl:when test="media:thumbnail">
	  <xsl:apply-templates select="media:thumbnail[1]" />
	</xsl:when>
	<xsl:otherwise>
	  <!-- FIXME explain condition -->
          <xsl:apply-templates select="enclosure[not(@length &lt; ../enclosure/@length)]"/>
	</xsl:otherwise>
      </xsl:choose>

      <!-- LINK PLAYER
      <xsl:variable name="podshow_bin">
	<xsl:choose>
	  <xsl:when test="starts-with(enclosure/@type,'image/')">podshow.php</xsl:when>
	  <xsl:when test="starts-with(enclosure/@type,'audio/')">podcastplayer.php</xsl:when>
	  <xsl:otherwise>podshow.php</xsl:otherwise>
	</xsl:choose>
      </xsl:variable>
      <a>
	<xsl:attribute name="href">/Podcast.bin/<xsl:value-of select="$podshow_bin"/>?cast=<xsl:value-of select="/rss/channel/atom:link/@href"/>&amp;n=<xsl:value-of select="position()"/></xsl:attribute>
	<img align="right"  border="0" src="/Podcast.bin/pics/play_icon.png"/>
      </a>
      -->

      <!-- headline -->
      <h4>
	<xsl:choose>
	  <xsl:when test="link">
	    <a>
	      <xsl:attribute name="href">
		<xsl:value-of select="normalize-space(link)"/>
	      </xsl:attribute>
	      <xsl:value-of select="title"/>
	    </a>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="title"/>
	  </xsl:otherwise>
	</xsl:choose>
      </h4>
      
      <!-- date -->
      <xsl:if test="pubDate">
	<p class="pubDate"><xsl:value-of select="pubDate"/></p>
      </xsl:if>
      <xsl:if test="*[local-name() = 'creator']">
	<p class="creator"><xsl:value-of select="*[local-name() = 'creator']"/></p>
      </xsl:if>

      <!-- description -->
      <xsl:choose>
	<xsl:when test="content:encoded">
	  <xsl:value-of select="content:encoded"/>
	</xsl:when>
	<xsl:when test="description">
	  <p class="description"><xsl:value-of select="description"/></p>
	</xsl:when>
      </xsl:choose>
      <xsl:if test="*[local-name() = 'abstract']">
        <p class="abstract">
          <xsl:value-of select="*[local-name() = 'abstract']"/>
        </p>
      </xsl:if>
      <!-- xsl:if test="content:encoded">
        <div class="content">
          <object type="text/html">
            <xsl:apply-templates select="content:encoded"/>
          </object>
        </div>
      </xsl:if -->

      <xsl:variable name="entryLocalName"><xsl:call-template name="substring-after-last">
	   <xsl:with-param name="string" select="guid"/>
	   <xsl:with-param name="char">/</xsl:with-param>
	 </xsl:call-template></xsl:variable>
      <button>
	<xsl:attribute name="onClick">doConfirmed(deleteEntry, '<xsl:value-of select="$entryLocalName"/>');</xsl:attribute>
        Delete
      </button>
    </div></li>
    <xsl:if test="position() mod 3 = 0"><div class="clearfix"/></xsl:if>
  </xsl:template>
  
  <xsl:template match="item" mode="iter">
    <div class="feed_item">
      <xsl:if test="guid">
        <xsl:attribute name="id">
          <xsl:value-of select="guid"/>
        </xsl:attribute>
      </xsl:if>
      <!--
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="link"/>
        </xsl:attribute>
        -->
        <b><xsl:value-of select="title"/></b>
        <!--
      </a>
      -->
      <font size="-2">
        <xsl:if test="description">
          <br/><xsl:value-of select="description"/>
        </xsl:if>
        <xsl:if test="*[local-name() = 'creator']">
          <br/>by <xsl:value-of select="*[local-name() = 'creator']"/>
        </xsl:if>
        <xsl:if test="pubDate">
          <br/>(<xsl:value-of select="pubDate"/>)
        </xsl:if>
      </font>
      <br/>
      <xsl:if test="pdccs:next">
        <span class="link">
          <xsl:attribute name="onClick">loadFeed('<xsl:value-of select="$xml"/>', 'SHOW=<xsl:value-of select="pdccs:next/@rdf:resource"/>')</xsl:attribute>
          next ...
        </span>
      </xsl:if>
      <div class="enclosure">
        <xsl:apply-templates select="enclosure" mode="iter" />
      </div>
    </div>
  </xsl:template>
  
  <xsl:template match="item" mode="index">
    <div class="feed_index_image">
      <xsl:if test="guid">
        <xsl:attribute name="id">
          <xsl:value-of select="guid"/>
        </xsl:attribute>
      </xsl:if>
      <!--
           <font size="-2">
             <xsl:if test="description">
               <br/><xsl:value-of select="description"/>
             </xsl:if>
             <xsl:if test="*[local-name() = 'creator']">
               <br/>by <xsl:value-of select="*[local-name() = 'creator']"/>
             </xsl:if>
             <xsl:if test="pubDate">
               <br/>(<xsl:value-of select="pubDate"/>)
             </xsl:if>
           </font>
           <br/>
      -->
      <xsl:apply-templates select="enclosure" mode="index" />
      <br/>
      <b><xsl:value-of select="title"/></b>
    </div>
    <xsl:variable name="NEXT">
      <xsl:value-of select="pdccs:next/@rdf:resource"/>
    </xsl:variable>
    <xsl:if test="not($NEXT = //pdccs:first/@rdf:resource)">
      <xsl:apply-templates select="../item[guid/text() = $NEXT]" mode="index"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="media:thumbnail">
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="../enclosure/@url"/>
      </xsl:attribute>
      <!--  style="max-width:100%;margin-top:10px;" border="0" -->
      <img>
	<xsl:attribute name="src">
	  <xsl:value-of select="@url"/>
	</xsl:attribute>
      </img>
    </a>
  </xsl:template>

  <xsl:template match="enclosure">
    <xsl:choose>
      <xsl:when test="starts-with(@type,'image/')">
	<a>
	  <xsl:attribute name="href">
            <xsl:value-of select="../enclosure[not(@length &lt; ../enclosure/@length)]/@url"/>
	  </xsl:attribute>
	  <img>
	    <xsl:attribute name="src">
              <xsl:value-of select="../enclosure[not(@length &gt; ../enclosure/@length)]/@url"/>
	    </xsl:attribute>
	  </img>
	</a>
      </xsl:when>
      <!--
      <xsl:when test="starts-with(@type,'video/')">
	<video  width="320" height="240" controls="controls">
          <source>
	    <xsl:attribute name="src">
	      <xsl:value-of select="@url"/>
	    </xsl:attribute>
	    <xsl:attribute name="type">
	      <xsl:value-of select="@type"/>
	    </xsl:attribute>
	  </source>
          This browser is not compatible with HTML 5
	  <br/>
	  Video:
	  <a>
	    <xsl:attribute name="href">
              <xsl:value-of select="@url"/>
	    </xsl:attribute>
	    <xsl:value-of select="@url"/>
	  </a>
	</video>
      </xsl:when>
      -->
      <xsl:otherwise>
	<a>
	  <xsl:attribute name="href">
            <xsl:value-of select="@url"/>
	  </xsl:attribute>
	  <xsl:value-of select="@url"/>
	</a>
	<xsl:if test="@type"> (<xsl:value-of select="@type"/>)</xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="enclosure" mode="iter">
    <img class="enclosure">
      <xsl:attribute name="src">
        <xsl:value-of select="@url"/>
      </xsl:attribute>
    </img>
  </xsl:template>

  <xsl:template match="enclosure" mode="index">
    <img class="enclosure">
      <xsl:attribute name="onClick">loadFeed('<xsl:value-of select="$xml"/>', 'SHOW=<xsl:value-of select="../guid"/>')</xsl:attribute>
      <xsl:attribute name="src">
        <xsl:value-of select="@url"/>
      </xsl:attribute>
    </img>
  </xsl:template>

  <xsl:template name="keynval">
    <ul>
      <b><xsl:value-of select="local-name()" />:</b>
      <xsl:choose>
        <xsl:when test="@xlink:href">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="@xlink:href" />
            </xsl:attribute>
            <xsl:value-of select="@xlink:href" />
          </a>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="./text()" />
        </xsl:otherwise>
      </xsl:choose>
      <br />
      <xsl:for-each select="./*">
        <xsl:call-template name="keynval" />
      </xsl:for-each>
    </ul>
  </xsl:template>

</xsl:stylesheet>
