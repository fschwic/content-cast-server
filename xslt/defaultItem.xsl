<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet 
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:dc="http://purl.org/dc/elements/1.1/" 
    xmlns:media="http://search.yahoo.com/mrss/" 
    xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
    >

  <!--
<item xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:media="http://search.yahoo.com/mrss/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">
  <guid isPermaLink="true">http://host:port/Bilder/D5/430_Ph/800/dsc_0376-800.jpg</guid>
  <title>dsc 0376-800</title>
  <description>synopsis of the story</description>
  <pubDate>Sat, 30 Apr 2011 14:55:22 +0200</pubDate>
  <enclosure url="http://host:port/Bilder/D50/1430_P/800/dsc6-800.jpg" type="image/jpeg" length="225280" />
  <source url="http://host:port/Bilder/D50/430_Pa/800/">800</source>
<media:thumbnail url="http://host:port/.derivates/Bilder/D5430_Pa/800/76-800.jpg.thmb" />
</item>
  -->

  <xsl:output 
      method="xml"
      encoding="UTF-8"
      />

  <xsl:template match="opml">
    <html>
      <xsl:apply-templates/>
    </html>
  </xsl:template>

  <xsl:template match="head">
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="body">
    <body>
      <h1>
        <img src="/Podcast.bin/pics/opml-icon-32x32.png" alt="opml file / directory"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="../head/title"/>
      </h1>
      <hr width="100%" size="1"/>
      <xsl:apply-templates select="outline">
        <xsl:sort select="@text"/>
      </xsl:apply-templates>
    </body>
  </xsl:template>

  <!-- erst Verzeichnisse, dann Podcasts
  <xsl:template match="body">
    <body>
      <h1>
        <img src="/mp3/pics/opml-icon-32x32.png" alt="opml file / directory"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="../head/title"/>
      </h1>
      <ul>
        <a href="#Verzeichnisse">Verzeichnisse</a> - <a href="#Feeds">Feeds</a>
      </ul>
      <hr width="100%" size="1"/>
      <a name="Verzeichnisse"/>
      <h3>Verzeichnisse</h3>
      <xsl:apply-templates select="outline[@type='link']"/>
      <a name="Feeds"/>
      <h3>Feeds</h3>
      <xsl:apply-templates select="outline[@type='rss']"/>
    </body>
  </xsl:template>
  -->

  <xsl:template match="outline[@type='include' or @type='link']">
    <img src="/Podcast.bin/pics/opml-icon-16x16.png" alt="opml file / directory"/>
    <xsl:text> </xsl:text>
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="@url"/>
      </xsl:attribute>
      <xsl:value-of select="@text"/>
    </a>
    <br/>
  </xsl:template>

  <xsl:template match="outline[@type='rss']">
    <xsl:choose>
      <xsl:when test="@version='RSS2' or @version='rss2'">
        <img src="/Podcast.bin/pics/feed-icon-14x14.png" alt="podcast"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="@type"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="@xmlUrl"/>
      </xsl:attribute>
      <xsl:value-of select="@text"/>
    </a>
    <br/>
  </xsl:template>

  <!--
  <xsl:template match="opml">
  </xsl:template>
  <xsl:template match="opml">
  </xsl:template>
  <xsl:template match="opml">
  </xsl:template>
  <xsl:template macht="opml">
  </xsl:template>
  -->

</xsl:stylesheet>

