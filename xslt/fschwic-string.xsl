<?xml version="1.0" encoding="UTF-8"?>
<!--
 String templates v0.1
 
 TODO XSLT to create HTML from Podcasts (RSS2 documents).
 Every item of the podcast should have a thumbnail defined (media:thumbnail).
 
 copyright 2011 Frank Schwichtenberg <http://frank.schwichtenberg.net>
 
 You may freely copy, use, and modify this XSLT if you don't remove my name
 and the origin location of this XSLT.
 
 The origin location of this XSLT is

 Here you may find new versions.
 -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  >

  <xsl:template name="substring-after-last">
    <xsl:param name="string"/>
    <xsl:param name="char"/>

    <xsl:variable name="rest" select="substring-after($string, $char)"/>

    <xsl:choose>
      <xsl:when test="$rest = ''">
	<xsl:value-of select="$string"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:call-template name="substring-after-last">
	  <xsl:with-param name="string" select="$rest"/>
	  <xsl:with-param name="char" select="$char"/>
	</xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

</xsl:stylesheet>
