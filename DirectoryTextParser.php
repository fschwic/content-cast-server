<?php

abstract class DirectoryTextElement {
  public $name;
}

class DirectoryTextContentElement extends DirectoryTextElement {
  public $value;

  public $attributes = array();

  function addAttribute($key, $value){
    $this->attributes[$key] = $value;
  }
}

class DirectoryTextDocument extends DirectoryTextElement {
  public $elements = array();

  function __construct() {
    $this->name = "ROOT";
  }

  function addElement($element){
    $this->elements[$element->name] = $element;
  }
}

class DirectoryTextParser {
  public $document;
  private $currentElement;

  function __construct() {
    $this->document = new DirectoryTextDocument();
  }

  function parse($file){

    if(! file_exists($file)){
      echo "File '".$file."' does not exist!\n";
      return;
    }
    $handle = fopen($file, "r");
    while (!feof($handle)) {
      $line = fgets($handle, 4096);
      #echo "###\n".$line."\n-------------------------------------------\n";
      if(strlen(trim($line)) != 0){
        $this->parseLine($line);
      }
    }
    fclose ($handle);
  }

  function parseLine($line){
    if(substr($line, 0, 1) == " "){
      $this->currentElement->value .= $line;
    }
    # TODO BEGIN and END
    else{
      $parts = preg_split("/(?<!\\\)[;\:]/", $line, -1, PREG_SPLIT_OFFSET_CAPTURE);
      #print_r($parts);
      $this->currentElement = new DirectoryTextContentElement();
      $this->currentElement->name = $parts[0][0];
      for($i=1; $i < count($parts); $i++){
        #echo "#".substr($line,$parts[$i][1]-1,1)."#\n";
        if(substr($line,$parts[$i][1]-1,1) == ";"){
	  list($key, $value) = split("=", $parts[$i][0], 2);
	  $this->currentElement->addAttribute($key, str_replace(array("\:", "\;"), array(":", ";"), $value));
	}
	else{
	  $this->currentElement->value = str_replace(array("\:", "\;"), array(":", ";"), trim($parts[$i][0]));
	}
      }
      $this->document->addElement($this->currentElement);
    }
  }

}

#$parser = new DirectoryTextParser();
#$parser->parse(".directory");
#echo "####################\n";
#print_r($parser->document);

?>
