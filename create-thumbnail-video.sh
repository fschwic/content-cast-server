#!/bin/bash

if [ "x$1" == "x" ] ; then
        echo Usage: $0 file
        exit 1;
fi

. PodcastDir.conf

FILE=$1
SIZE="400"

file_name=`basename "$FILE"`
file_rel_path=${FILE/$BASE_DIR/}
thumb_file=${DERIVATES_DIR}${file_rel_path}.thmb
thumb_file_dir=${DERIVATES_DIR}${file_rel_path/$file_name/}
thumb_url=${DERIVATES_URL}${file_rel_path}.thmb

${BIN_MKDIR} -p "$thumb_file_dir"

for S in ${SIZE}; do
if [ ! -e "${thumb_file}" ] ; then
    ${BIN_MPLAYER} -nosound -frames 1 -vo jpeg:outdir=/tmp/ -ss 3 -zoom -vf scale=${S}:-3 "$FILE" 1>&2 
    ${BIN_MV} -f /tmp/00000001.jpg "$thumb_file"
    ${BIN_IMAGEMAGICK_COMPOSITE} -gravity center ${BIN_DIR}/pics/Play.gif "$thumb_file" "$thumb_file"
fi
done

echo $thumb_url
