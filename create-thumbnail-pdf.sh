#!/bin/bash

if [ "x$1" == "x" ] ; then
        echo Usage: $0 file
        exit 1;
fi

. PodcastDir.conf

FILE=$1
SIZE="400"
THUMB_FILE_EXTENSION=".thmb.gif"

file_name=`$BIN_BASENAME "$FILE"`
file_rel_path=${FILE/$BASE_DIR/}
thumb_file=${DERIVATES_DIR}${file_rel_path}${THUMB_FILE_EXTENSION}
thumb_file_dir=${DERIVATES_DIR}${file_rel_path/$file_name/}
thumb_url=${DERIVATES_URL}${file_rel_path}${THUMB_FILE_EXTENSION}

$BIN_MKDIR -p "$thumb_file_dir"

for S in ${SIZE}; do
    $BIN_IMAGEMAGICK_CONVERT -thumbnail ${S} -delay 100 "${FILE}" "$thumb_file"
done

echo $thumb_url
