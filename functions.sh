#!/bin/bash

. PodcastDir.conf

function mimeType {
    $BIN_FILE -b --mime-type "$1"
}

function pubDate {
    tmp=$LANG
    LANG=en_US.UTF-8
    mime=`mimeType "$1"`
    filename=`$BIN_BASENAME "$1" `
    offset=""
    if [ "x$2" != "x" ]; then
	offset="$2 minutes ago"
    fi
    #echo $offset 1>&2
echo $filename >&2;
    if [ "${filename:0:4}" = "VID_" ]; then
        # VID_20110416_170331.3gp 
        year=${filename:4:4}
	month=${filename:8:2}
	day=${filename:10:2}
	hour=${filename:13:2}
	minute=${filename:15:2}
	second=${filename:17:2}
	# Thu, 14 Apr 2011 13:35:30 +0200 

	$BIN_DATE -d "${year}-${month}-${day} ${hour}:${minute}:${second} $offset" +"%a, %e %b %Y %T %z"

    elif [ ${mime/\/*/} = "image" ]; then
	datetimestamp=`$BIN_IMAGEMAGICK_IDENTIFY -format "%[EXIF:DateTimeDigitized]" "$1"`
	datetimearray=($datetimestamp)
	datestamp=${datetimearray[0]}
	$BIN_DATE -d "${datestamp//:/-} ${datetimearray[1]} $offset" +"%a, %e %b %Y %T %z"
    else
	#$BIN_LS -l --time-style="+%a, %e %b %Y %T %z" $1 | awk '{print $6 " " $7 " " $8 " " $9 " " $10 " " $11}'
	$BIN_DATE -d "now $offset" +"%a, %e %b %Y %T %z"
    fi
    LANG=$tmp
}

function length {
    line=`$BIN_LS -s --block-size=1 "$1"`
    array=($line)
    echo ${array[0]}
}

declare -A DIRECTORY
function readDirectoryText {
    DIRECTORY=()
    tmp=$IFS
    
    directory_wo_base=${1/${BASE_DIR}\//}
    directory=${BASE_DIR}
    IFS=/
    for folder in $directory_wo_base; do
	directory="${directory}/$folder"
	if [ -e "${directory}/.directory" ]; then
	    IFS=:
            # TODO for every parent directory starting with BASE_DIR
            # possible settings from parent directory will be overwritten
	    while read line; do
	      # TODO ignore attributes (;) and don't spilt when escaped (\:)
		a=($line)
		if [ ! -z ${a[0]} ] ; then
		    trimed=$(echo ${a[1]} | sed 's/ *$//g')
		    DIRECTORY[${a[0]}]=${trimed}
		fi
	    done < "${directory}/.directory"
        fi
    done
    IFS=$tmp
}
