#!/bin/bash

if [ "x$6" == "x" ] ; then
        echo Usage: $0 title description date enclosure_url enclosure_type enclosure_length podcast_url thumbnail_url
        exit 1;
fi

TITLE=$1
DESCRIPTION=$2
DATE=$3
ENCLOSURE_URL=$4
ENCLOSURE_TYPE=$5
ENCLOSURE_LENGTH=$6
PODCAST_URL=$7
THUMBNAIL_URL=$8

echo "<item xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:media=\"http://search.yahoo.com/mrss/\" xmlns:itunes=\"http://www.itunes.com/dtds/podcast-1.0.dtd\">
  <guid isPermaLink=\"true\">${ENCLOSURE_URL}</guid>
  <title>${TITLE}</title>"
if [ "x$DESCRIPTION" != "x" ] ; then
    echo "  <description>${DESCRIPTION}</description>"
fi
echo "  <pubDate>${DATE}</pubDate>
  <enclosure url=\"${ENCLOSURE_URL}\" type=\"${ENCLOSURE_TYPE}\" length=\"${ENCLOSURE_LENGTH}\" />"
if [ "x$PODCAST_URL" != "x" ] ; then
    echo "  <source url=\"${PODCAST_URL}\">Origin ContentCast</source>"
fi
if [ "x$THUMBNAIL_URL" != "x" ] ; then
    echo "  <media:thumbnail url=\"${THUMBNAIL_URL}\" />"
fi
echo "</item>"
