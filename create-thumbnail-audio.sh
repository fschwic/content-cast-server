#!/bin/bash

if [ "x$1" == "x" ] ; then
        echo Usage: $0 file
        exit 1;
fi

. PodcastDir.conf

cover="${1/$(basename $1)/}cover.jpg"
if [ ! -e "$cover" ]; then
    cover="${1/$(basename $1)/}cover.png"
fi
if [ -e "$cover" ]; then
#    #${cover/$BASE_DIR/$BASE_URL}
#    file_rel_path=${cover/$BASE_DIR/}
#    thumb_file=${DERIVATES_DIR}${file_rel_path}.thmb
#    thumb_url=${DERIVATES_URL}${file_rel_path}.thmb
#    $BIN_IMAGEMAGICK_CONVERT -size 200x200 "$cover" -resize 200x200 $ROTATION "$thumb_file"
#    echo $thumb_url
    ./create-thumbnail-image.sh "$cover"
else
    echo ${BASE_URL}"/Podcast.bin/pics/mime-audio.gif"
fi

