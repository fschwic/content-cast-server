#!/bin/bash

if [ "x$1" == "x" ] ; then
        echo Usage: $0 file
        exit 1;
fi

. PodcastDir.conf

FILE=$1
SIZE="400 200 120 800"

file_name=`$BIN_BASENAME "$FILE"`
file_rel_path=${FILE/$BASE_DIR/}
thumb_file=${DERIVATES_DIR}${file_rel_path}.thmb
thumb_file_dir=${DERIVATES_DIR}${file_rel_path/$file_name/}
# FIXME just spaces urlencodedx
thumb_url=${DERIVATES_URL}${file_rel_path/$file_name/${file_name//\ /%20}}.thmb

ROTATION=""
function rotation {
      ORIENTATION=`$BIN_IMAGEMAGICK_IDENTIFY -format "%[EXIF:Orientation]" "$1"`
      echo -n ", Orientation "${ORIENTATION} 1>&2
      ROTATION=""
      case "${ORIENTATION}" in
	  1)
	      #echo " don't rotate " 1>&2
	      ;;
	  3)
	      echo " rotate 180°" 1>&2
	      ROTATION=" -rotate 180 "
	      ;;
	  6)
	      echo " rotate 90°" 1>&2
	      ROTATION=" -rotate 90 "
	      ## and delete all profiles (incl. "Orientation") cause we 
	      ## do not know how to set the new orientation
	      #convert -rotate 90 $i +profile "*" ${i}-90
	      #mv ${i}-90 $i
	      ;;
	  8)
	      echo " rotate 270°" 1>&2
	      ROTATION=" -rotate 270 "
	      ## and delete all profiles (incl. "Orientation") cause we 
	      ## do not know how to set the new orientation
	      #convert -rotate 270 $i +profile "*" ${i}-270
	      #mv ${i}-270 $i
	      ;;
	  *)
	      echo " can not get Orientation '$ORIENTATION'" 1>&2
	      ;;
      esac
}
#rotation "$FILE"
ROTATION=" -auto-orient "
$BIN_MKDIR -p "$thumb_file_dir"

for S in ${SIZE}; do
if [ ! -e "${thumb_file}.${S}" ] ; then
    $BIN_IMAGEMAGICK_CONVERT -size ${S}x${S} "$FILE" -resize ${S}x${S} $ROTATION "${thumb_file}.${S}"
fi
done

echo "${thumb_url}.400"
