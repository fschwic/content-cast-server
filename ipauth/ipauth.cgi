#!/bin/bash

. ../PodcastDir.conf

HTACCESS=${BASE_DIR}/.htaccess

echo "Content-type: text/html"
echo ""

echo '
<html> 
<head> 
<meta  name="robots" content="noindex, nofollow" /> 
<!-- meta http-equiv="refresh" content="6; URL=/Podcast/" --> 
<title>IPAuth</title>
</head>
'
echo "Your IP is ${REMOTE_ADDR}."
echo "<br/>"

now_seconds=$(${BIN_DATE} +%s)
#echo "Now: ${now_seconds}."

${BIN_AWK} -v ip=${REMOTE_ADDR} -v now=${now_seconds} -v offset=${IPAUTH_TIMEOUT} '
  $1 != "SetEnvIf" && $1 != "#END" {
    print $0
  }; 
  
  $1 == "SetEnvIf" {
    if ($6 + offset < now) {
    } 
    else { 
      if ($3 == ip) {
        already_in = 1
      }; 
      print $0 
    }
  }; 
  
  $1 == "#END" { 
    if(!already_in) { 
      print "SetEnvIf Remote_Addr " ip " ipauth # " now 
    }; 
    print "#END" 
  }
' ${HTACCESS} > /tmp/PodcastDir.htaccess

cp -f /tmp/PodcastDir.htaccess ${HTACCESS}

echo "<h2>You are allowed to access the requested resource for at least ${IPAUTH_TIMEOUT} seconds.</h2>"
echo "<br/>"
echo "<i><b>You will be redirected in a few seconds!</b></i>"
echo "<br/>"
echo "<br/>"

echo "<pre>" 
echo "### ${HTACCESS} #####################################"
cat ${HTACCESS}
echo "#####################################################"
echo "</pre>"

echo "</html>"
