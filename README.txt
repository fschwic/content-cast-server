=== Installation === 

DRAFT install instruction are very incomplete, till now.

Choose a BASE_DIR for your data. Good idea might be a mounted physical
filesystem.

Create a directory Podcast.bin in BASE_DIR and copy all files from
this project into that directory.

Place all your data in Directories located in BASE_DIR.

Generated Podcasts will be placed in directory Podcast inside
BASE_DIR.

Configure Apache Web Server to deliver data from BASE_DIR when
accessing /.

Create a link in ${BASE_DIR}/Podcast named index.php pointing to 
../Podcast.bin/index.php-PodcastDir.

Move PodcastDir.conf.example to PodcastDir.conf and configure BASE_DIR
and BASE_URL.

=== Generate Podcast Directory ===

sudo su - -c 'su -c "cd /media/Hektor/Podcast.bin ;\
./generate-podcast-directory.sh Hoerspiele/Die_drei_Fragezeichen"
www-data'

=== .directory files ===

Pseudo ICS format: key and value are in one line separated by a colon.
E.g.

 overwrite:all

;overwrite 
:''all'' or ''none''. Default is ''none'' which means when
 generating rss entries for this directory already existing ones are
 not touched. If set to ''all'' for every file a new rss entry is
 generated overwriting changes that where made in these entries.

;visibility 
: Possible value is ''private''. If not set or set to
 something else as private it is ignored. If set to private the
 directory is delivered only if individual credentials are present.

;title
:A title for the directory used as title for the rss channel.

;description
:A description for the directory used as description for the rss channel.

;link
:A URL used as link of the rss channel. Default is the URL of the channel itself.

;image 
:The URL of an image which is used as image for the rss
 channel. Default is a cover.jpg if found in the directory otherwise no
 image.

;keep-numbers 
: ''yes'' or ''no''. Meant for audio plays where the
 number in the title is important. Either for knowing which volume it
 is (e.g. Drei Fragezeichen Nr. 148) or for recognize parts when there
 are a lot of files with the title of the play/book prefixed with a
 number.

=== Access ===

By default XML is delivered following the RSS2 (which depends on the
generated snippets, of course) and OPML format. For both a stylesheet
processing instruction is included in the XML for transforming the XML
into HTML. So, a browser is able to convert the XML for human
readers. Because some browsers does not support XSL transformation for
RSS there are two possibilities to force Content Cast Server to
deliver HTML. Either one removes the trailing slash ('/') from the URL
and suffix it with '.html' or an HTTP Accept header must be send where
'text/html' is preferred over 'application/xml' and
'application/rss+xml'. For internal transformation the commandline
tool xsltproc must be configured and available.

=== TODO ===

* images for entries. For thumbnail a JPG with the same basename then
  the content file should be tried to be found. Especially useful for
  audio files. For images a thumbnail is created from the original
  image.

* .directory with ICS attributes and linebreakes. Support title
  etc. in different languages. title;lang=en:The Title
