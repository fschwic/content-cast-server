#!/bin/bash

. PodcastDir.conf; 
. functions.sh; 

IFS=$'\n'; 
for i in `find ${BASE_DIR}/$1 -name *.jpg`; 
do 
	file_name=`basename $i`; 
	file_rel_path=${i/$BASE_DIR/}; 
	file_dir=${i/$file_name/}; 

	file_title=${file_name/\.*/}
	file_title=${file_title/_/\ }
	file_url=${BASE_URL}${file_rel_path}; 
	rss2_file=${PODCAST_DIR}${file_rel_path}.rss2; 
	rss2_file_dir=${PODCAST_DIR}${file_rel_path/$file_name/}
	
	# create directory for RSS snippets
	mkdir -p $rss2_file_dir
	# write RSS2 snippet
	echo -n '<item xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">
  <guid isPermaLink="true">'$file_url'</guid>
  <title>'$file_title'</title>
  <description></description>
  <pubDate>'`pubDate $i`'</pubDate>
  <enclosure url="'$file_url'" type="'`mimeType $i`'" length="'`length $i`'" />
</item>' > $rss2_file

done

