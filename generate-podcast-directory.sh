#!/bin/bash

. PodcastDir.conf

IFS=$'\n'; 
for f in `$BIN_FIND ${BASE_DIR}/$1 -type f`; 
do
    filename=`$BIN_BASENAME "$f"`
    exclude=false
    for excludefilename in "${EXCLUDE_FILES[@]}";
    do
        if [ $filename = $excludefilename ]; then
	    exclude=true
	    break
        fi
    done
    if [ "$exclude" = "true" ]; then
        continue
    fi
    if [ ${filename:0:1} != "." ]; then
	$BIN_DIR/generate-rss2-item.sh "$f"
    fi
done
